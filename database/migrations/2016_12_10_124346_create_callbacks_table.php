<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('callbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id');
            $table->string('link');
            $table->text('text');
            $table->string('author_avatar');
            $table->string('author_name');
            $table->string('action');
            $table->string('sub_action');
            $table->string('unix_data')->nullable();
            $table->string('status');
            $table->boolean('favorite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('callbacks');
    }
}

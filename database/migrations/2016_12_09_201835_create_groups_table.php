<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('avatar');
            $table->string('callback_link');
            $table->integer('group_id');
            $table->integer('owner_id');
            $table->text('black_list')->default(null);
            $table->boolean('notify_comment')->default(false);
            $table->boolean('notify_message')->default(false);
            $table->boolean('notify_wall')->default(false);
            $table->boolean('notify_board')->default(false);
            $table->boolean('notify_group')->default(false);
            $table->string('response_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groups');
    }
}

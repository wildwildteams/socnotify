<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('community_id');
            $table->integer('owner_id');
            $table->text('black_list')->nullable();
            $table->boolean('send_sms')->nullable();
            $table->boolean('send_email')->nullable();
            $table->boolean('send_telegram')->nullable();
            $table->boolean('send_push')->nullable();
            $table->string('sms_num')->nullable();
            $table->string('email')->nullable();
            $table->string('telegram_code')->nullable();
            $table->string('telegram_login')->nullable();
            $table->string('telegram_id')->nullable();
            $table->string('push_api_data')->nullable();
            $table->time('sleep_from')->nullable();
            $table->time('sleep_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}

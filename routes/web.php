<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('landing.home');
});

Auth::routes();

Route::get('auth/vkontakte', 'Auth\AuthController@redirectToProvider');
Route::get('auth/vkontakte/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/home', 'HomeController@index');
Route::get('/home/groups', 'HomeController@ShowGroupsPage');
Route::get('/home/groups/settings/{id}', 'HomeController@ShowGroupSettings');
Route::get('/home/groups/show/stats/{id}', 'HomeController@ShowGroupById');
Route::get('/home/groups/notify/{type}/{id}/{status}', 'HomeController@ShowNotification');
Route::get('/home/stats', 'HomeController@ShowStatsPage');
Route::get('/home/notify', 'HomeController@ShowNotifyPage');
Route::get('/home/tariff', 'HomeController@ShowTariffPage');
Route::get('/telegram', 'CallBackController@TelegramTest');

Route::get('callback/pushall/web', 'CallBackController@SetPushAllData');

Route::post('/ajax/AddGroup', 'GroupsController@GroupAdd');
Route::post('/ajax/UpdateGroup', 'GroupsController@UpdateGroupSettings');
Route::post('/ajax/DeleteGroupById', 'GroupsController@DeleteGroupById');
Route::post('/ajax/WorkWithCallback', 'CallBackController@Worker');
Route::post('/ajax/PayForTariff', 'TariffController@PayForTariff');
Route::post('/ajax/GetGift', 'GiftController@GetGift');



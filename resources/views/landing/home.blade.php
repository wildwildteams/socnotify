<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SocNotify - сервис для автоматизации продаж в социальных сетях</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="telderi" content="25a975d0e13c1887c3e7d4ed7dbf0081" />
    <link rel="stylesheet" href="{{ secure_asset('landing/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('landing/css/skeleton.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('landing/css/main.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('landing/css/price-table.css') }}">
    <script src="https://use.fontawesome.com/f60b82d128.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,700" rel="stylesheet">
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>
</head>
<body>
<div id="menu">
    <div class="container">
        <div class="row">
            <div class="four columns">
                <img class="logo" src="{{ secure_asset('landing/img/logo.png') }}" />
            </div>
            <div class="four columns">
                <a class="button" href="{{ secure_url('/auth/vkontakte') }}">Подключить группу</a>
            </div>
            <div class="four columns">
                <div class="contact">
                    <div class="skype"><i class="fa fa-skype" aria-hidden="true"></i> live:wildwildteams_2</div>
                    <div class="telegram"><i class="fa fa-telegram" aria-hidden="true"></i> wildwildteams</div>
                    <div class="mail"><i class="fa fa-envelope" aria-hidden="true"></i> ask@socnotify.online</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="second-block">
    <div class="container">
        <div class="row">
            <div class="three columns">
                <div class="info">Повышает конверсию группы ВКонтаке</div>
                <div class="info">Экономит время администратора</div>
            </div>
            <div class="six columns">
                <h3>Онлайн сервис для<br> мониторинга комментариев<br> <i class="fa fa-vk" aria-hidden="true"></i> ВКонтакте</h3>
                <p>Начните получать уведомления от вашего сообщества ВКонтакте через <b>SMS</b>, <b>Email</b>, <b>Telegram</b>, <br>а так же <b>уведомления на Android, IOS</b></p>
                <div style="width: 200px; margin: 0 auto;"><hr noshade></div>
                <p>Проверенно ведущими интернет магазинами</p>
            </div>
            <div class="three columns">
                <div class="info">Повышает скорость обслуживания клиентов</div>
                <div class="info">Круглосуточная работа</div>
            </div>
        </div>
        <div class="row">
            <div class="six columns">
                <ul>
                    <li><i class="fa fa-credit-card-alt" aria-hidden="true"></i> 3 дня бесплатно, без обязательных начальных платежей</li>
                    <li><i class="fa fa-globe" aria-hidden="true"></i> Полный функционал, без ограничений</li>
                    <li><i class="fa fa-shield" aria-hidden="true"></i> Ваши данные надежно защищены</li>
                </ul>
            </div>
            <div class="six columns">
                <div class="free-block">
                    Получите 3 дня бесплатного использования нашего сервиса, после бесплатной регистрации.<br>
                    <a class="button" href="{{ secure_url('/auth/vkontakte') }}">Получить</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="thirth-block">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <h1>Результаты внедрения онлайн-сервиса протестированы
                    и подтверждены ниже перечисленными <b>магазинами:</b></h1>
                <img class="clients" src="http://karapuzov.com.ua/image/cache/karapuzov-logo-480x240.png" width="200">
                <img class="clients" src="http://www.mamindom.ua/img/logo-footer.png" width="200">
                <img class="clients" src="http://amideya.com.ua/skin/frontend/ultimo/default/images/logo.png" width="220">
            </div>
        </div>
    </div>
</div>
<div id="fourth-block">
    <div class="container">
        <div class="row">
            <div class="twelve">
                <h1>Про SocNotify</h1>
            </div>
        </div>
        <div class="row" style="text-align: left;">
            <p><b>SocNotify</b> – это сервис получения моментальных уведомлений о различных событиях Вашего сообщества Вконтакте. Функции сервиса позволяют экономить время и увеличивать уровень взаимодействия с аудиторией. </p>
    <h4>Как работает SocNotify?</h4>
<p>Наш инструмент предназначен для упрощения работы SMM-специалистам и владельцам коммерческих групп. Подключив SocNotify к своему сообществу, Вы будете получать уведомления о различных действиях посетителей:
<ul>
    <li>Комментарии – при наличии новых комментариев</li>
    <li>Сообщения – при получении сообщения сообществу</li>
    <li>Обсуждения – при ответах в опубликованных темах обсуждений</li>
    <li>Записи – при публикации записи в сообществе</li>
</ul></p>
<h4>Преимущества SocNotify</h4>
<p>Получая моментальные  уведомления, у Вас будет возможность не только следить за активностью в своей группе, но и своевременно реагировать на действия пользователей: отвечать клиентам на вопросы, консультировать посетителей, дополнять или исправлять их высказывания. 
<p>Сервис SocNotify станет незаменимым помощником при наличии нескольких групп, за которыми нужно регулярно следить, обновлять и общаться с аудиторией. </p>
Наш инструмент позволяет получать уведомления разными способами:
<ul>
    <li>На Ваш Email</li>
    <li>В Telegram</li>
    <li>Посредством SMS сообщений</li>
    <li>Посредством PUSH уведомлений</li>
</ul></p><p>
Если Вы цените свое время и желаете упросить процесс мониторинга своего сообщества, воспользуйтесь функционалом SocNotify. На протяжении 3 дней Вы сможете бесплатно опробовать все возможности нашего проекта. </p>

        </div>
        <div class="row">
            <div class="twelve">
                <h1>Возможности сервиса SOCNOTIFY</h1>
            </div>
        </div>
        <div class="row">
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/comment.png') }}" width="60" align="center"/><br>
                    <b>Отслеживание комментариев</b>
                    <p>Отслеживание и уведомление вас о новых коментариях в сообществе</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/message.png') }}" width="60" align="center"/><br>
                    <b>Отслеживание сообщений</b>
                    <p>Получайте уведомления о новых сообщениях для вашего сообщества</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/board.png') }}" width="60" align="center"/><br>
                    <b>Отслеживание обсуждений</b>
                    <p>Получайте уведомления о новых обсуждениях и сообщениях в уже созданных обсуждениях</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/post.png') }}" width="60" align="center"/><br>
                    <b>Отслеживание записей</b>
                    <p>Получайте уведомления о новых записях в вашем сообществе</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/email.png') }}" width="60" align="center"/><br>
                    <b>Уведомления на Email</b>
                    <p>Получайте уведомления на свою электронную почту</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/telegram.png') }}" width="60" align="center"/><br>
                    <b>Уведомления в Telegram</b>
                    <p>Получайте уведомления на свою электронную почту</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="{{ secure_asset('landing/img/sms.png') }}" width="60" align="center"/><br>
                    <b>Уведомления в SMS</b>
                    <p>Получайте уведомления на свою свой мобильный телефон</p>
                </div>
            </div>
            <div class="three columns">
                <div class="settings">
                    <img src="http://www.gr8appdesign.com/wp-content/uploads/2016/04/push-notification.png" width="60" align="center"/><br>
                    <b>Уведомления PUSH</b>
                    <p>Получайте уведомления в браузере, и на телефонах с Android и IOS</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="twelve columns">
                <h1>Цена?</h1>
            </div>
        </div>
        <div class="row">
            <div class="four columns">
                <h2><u>1 Сообщество</u></h2>
                <h2><u>249р</u></h2>
                <ul>
                    <li>Количество сообществ: <b>1</b></li>
                    <li>Уведомления на Email</li>
                    <li>Уведомления в Telegram</li>
                    <li>Уведомления с помощью SMS</li>
                    <li>Уведомления push</li>
                </ul>
            </div>
            <div class="four columns">
                <h2><u>3 Сообщества</u></h2>
                <h2><u>549р</u></h2>
                <ul>
                    <li>Количество сообществ: <b>3</b></li>
                    <li>Уведомления на Email</li>
                    <li>Уведомления в Telegram</li>
                    <li>Уведомления с помощью SMS</li>
                    <li>Уведомления push</li>
                </ul>
            </div>
            <div class="four columns">
                <h2><u>Безлимит</u></h2>
                <h2><u>849р</u></h2>
                <ul>
                    <li>Количество сообществ: <b>безлимитно</b></li>
                    <li>Уведомления на Email</li>
                    <li>Уведомления в Telegram</li>
                    <li>Уведомления с помощью SMS</li>
                    <li>Уведомления push</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="fifth-block">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <h1>FAQ/Часто задаваемые вопросы</h1>
                <h6>Подарочная версия используется 3 дней, а платная версия — на сколько?</h6>
                <p>На нашем сервисе вы оплачиваете подписку на 31 день использования. Так же есть возможность оптовой покупки доступов со скидкой, для этого вам стоит написать нам по указанным на сайте контактам.</p>

                <h6>Онлайн-сервисом можно пользоваться только на одном устройстве?</h6>
                <p>Вы можете работать на любом ПК, ноутбуке или планшете в любом браузере. Нет необходимости покупать отдельные доступы для разных устройств. Просто используете свой логин и пароль при входе и система загружает уже имеющиеся данные по вашим группам.</p>

                <h6>Есть ли лимит на количество уведомлений в сутки?</h6>
                <p>Ограничений нет.</p>

                <h6>Как пополнить баланс на сайте?</h6>
                <p>Пополнение происходит через сервис Яндекс.Деньги, и позволяет оплачивать с помощью яндекс денег, банковских карт, а так же смс сообщений.</p>

                <h6>Как быстро приходят уведомления?</h6>
                <p>Уведомления отправляются в фоновом режиме, в порядке очереди. Это позволяет их отправлять адресату моментально, без нагрузки на систему, и панель управления.</p>

                <h6>Хочу купить ваш сервис, это возможно?</h6>
                <p>Сервис SocNotify не продается, однако вы можете открыть сервис по нашей франшизе. Для получения подробной информации обращайтесь по указанным на сайте контактам.</p>

            </div>
        </div>
    </div>
</div>
<div id="sixth-block">
    <div class="container">
        <div class="row">
            <div class="six columns">
                <img style="padding: 25px;" src="{{ secure_asset('landing/img/logo.png') }}" />
            </div>
            <div class="six columns">
               <h1>Присоединяйся к нам</h1>
                <div id="vk_groups" style="position: relative; top: -8px;"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 4, wide: 1, width: "348", height: "400"}, 133937532);
                </script>
            </div>
        </div>
    </div>
</div>
<div id="seventen-block">
    <div class="container">
        <div class="row">
            <div class="six columns">Все права защищены. SocNotify 2016-2017</div>
            <div class="six columns">Сделанно с любовью благодаря WildWildTeams</div>
        </div>
    </div>
</div>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>

<!-- VK Widget -->
<div id="vk_community_messages"></div>
<script type="text/javascript">
VK.Widgets.CommunityMessages("vk_community_messages", 133937532, {expanded: "1",tooltipButtonText: "Есть вопрос?"});
</script>
</body>
</html>
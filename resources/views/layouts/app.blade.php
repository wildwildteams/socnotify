
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>SocNotify - отслеживание комментариев ВКонтакте</title>
    <meta name="description" content="Responsive, Bootstrap, BS4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

    <!-- style -->
    <link rel="stylesheet" href="{{ secure_asset('css/animate.css/animate.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/glyphicons/glyphicons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/material-design-icons/material-design-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/ionicons/css/ionicons.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/toastr/toastr.css') }}" type="text/css" />
    <!-- build:css css/styles/app.min.css -->
    <link rel="stylesheet" href="{{ secure_asset('css/styles/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ secure_asset('css/styles/style.css') }}" type="text/css" />
    <!-- endbuild -->
    <link rel="stylesheet" href="{{ secure_asset('css/styles/font.css') }}" type="text/css" />
</head>
<body>
<div class="app" id="app">

    <!-- ############ LAYOUT START-->

    <!-- aside -->
    <div id="aside" class="app-aside fade nav-dropdown black">
        <!-- fluid app aside -->
        <div class="navside dk" data-layout="column">
            <div class="navbar no-radius">
                <!-- brand -->
                <a href="{{ secure_url('/home') }}" class="navbar-brand">
                    <img src="{{ secure_url('landing/img/logo.png') }}">
                </a>
                <!-- / brand -->
            </div>
            <div data-flex class="hide-scroll">
                <nav class="scroll nav-stacked nav-stacked-rounded nav-color">

                    <ul class="nav" data-ui-nav>
                        <li class="nav-header hidden-folded">
                            <span class="text-xs">Основные</span>
                        </li>

                        <li>
                            <a href="{{ secure_url('home') }}" class="b-info">
                  <span class="nav-icon text-white no-fade">
                    <i class="ion-compose"></i>
                  </span>
                                <span class="nav-text">Новости</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ secure_url('home/groups') }}" class="b-info">
                  <span class="nav-icon text-white no-fade">
                    <i class="ion-ios-people"></i>
                  </span>
                                <span class="nav-text">Группы</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ secure_url('home/stats') }}" class="b-info">
                  <span class="nav-icon text-white no-fade">
                    <i class="ion-stats-bars"></i>
                  </span>
                                <span class="nav-text">Статистика</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ secure_url('home/tariff') }}" class="b-info">
                  <span class="nav-icon text-white no-fade">
                    <i class="ion-cash"></i>
                  </span>
                                <span class="nav-text">Тарифы</span>
                            </a>
                        </li>
                        <li>
                            <a href="http://SocNotify.reformal.ru" onclick="Reformal.widgetOpen();return false;" onmouseover="Reformal.widgetPreload();" class="b-info">
                  <span class="nav-icon text-white no-fade">
                    <i class="ion-android-notifications"></i>
                  </span>
                                <span class="nav-text">Предложить идею</span>
                            </a>
                        </li>
                        
                </nav>
            </div>
        </div>
    </div>
    <!-- / -->

    <!-- content -->
    <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
        <div class="app-header white bg b-b">
            <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                    <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Панель управления</div>
                <!-- nabar right -->
                <ul class="nav navbar-nav pull-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link clear" data-toggle="dropdown">
                      <span class="avatar w-32">
                        <img src="{{ Auth::user()->avatar }}" class="w-full rounded" alt="...">
                      </span>
                        </a>
                    </li>
                </ul>
                <!-- / navbar right -->
            </div>
        </div>
        <div class="app-footer white bg p-a b-t">
            <div class="pull-right text-sm text-muted">Version 1.0.0</div>
            <span class="text-sm text-muted">SocNotify &copy; Copyright 2017-2018.</span>
        </div>
        <div class="app-body">

            <!-- ############ PAGE START-->
            <div class="row-col">
                <div class="col-lg b-r">
                    <div class="padding">
                        <div class="row">
                            <div class="col-sm-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade inactive" id="chat" data-backdrop="false">
                <div class="modal-right w-xxl dark-white b-l">
                    <div class="row-col">
                        <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">&times;</a>
                        <div class="p-a b-b">
                            <span class="h5">Chat</span>
                        </div>
                        <div class="row-row light">
                            <div class="row-body scrollable hover">
                                <div class="row-inner">
                                    <div class="p-a-md">
                                        <div class="m-b">
                                            <a href="#" class="pull-left w-40 m-r-sm"><img src="images/a2.jpg" alt="..." class="w-full img-circle"></a>
                                            <div class="clear">
                                                <div class="p-a p-y-sm dark-white inline r">
                                                    Hi John, What's up...
                                                </div>
                                                <div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i> 2 minutes ago</div>
                                            </div>
                                        </div>
                                        <div class="m-b">
                                            <a href="#" class="pull-right w-40 m-l-sm"><img src="images/a3.jpg" class="w-full img-circle" alt="..."></a>
                                            <div class="clear text-right">
                                                <div class="p-a p-y-sm success inline text-left r">
                                                    Lorem ipsum dolor soe rooke..
                                                </div>
                                                <div class="text-muted text-xs m-t-xs">1 minutes ago</div>
                                            </div>
                                        </div>
                                        <div class="m-b">
                                            <a href="#" class="pull-left w-40 m-r-sm"><img src="images/a2.jpg" alt="..." class="w-full img-circle"></a>
                                            <div class="clear">
                                                <div class="p-a p-y-sm dark-white inline r">
                                                    Good!
                                                </div>
                                                <div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i> 5 seconds ago</div>
                                            </div>
                                        </div>
                                        <div class="m-b">
                                            <a href="#" class="pull-right w-40 m-l-sm"><img src="images/a3.jpg" class="w-full img-circle" alt="..."></a>
                                            <div class="clear text-right">
                                                <div class="p-a p-y-sm success inline text-left r">
                                                    Dlor soe isep..
                                                </div>
                                                <div class="text-muted text-xs m-t-xs">Just now</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-a b-t">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Say something">
  	          <span class="input-group-btn">
  	            <button class="btn white b-a no-shadow" type="button">SEND</button>
  	          </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->

    <!-- ############ LAYOUT END-->
</div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
<script src="{{ secure_asset('libs/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ secure_asset('libs/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ secure_asset('libs/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- core -->
<script src="{{ secure_asset('libs/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
<script src="{{ secure_asset('libs/PACE/pace.min.js') }}"></script>
<script src="{{ secure_asset('libs/jquery-pjax/jquery.pjax.js') }}"></script>
<script src="{{ secure_asset('libs/blockUI/jquery.blockUI.js') }}"></script>
<script src="{{ secure_asset('libs/jscroll/jquery.jscroll.min.js') }}"></script>
<script src="{{ secure_asset('libs/toastr/toastr.min.js') }}"></script>

<script src="{{ secure_asset('scripts/core.js') }}"></script>
<script src="{{ secure_asset('scripts/config.lazyload.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-load.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-jp.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-include.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-device.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-form.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-modal.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-nav.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-list.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-screenfull.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-scroll-to.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-toggle-class.js') }}"></script>
<script src="{{ secure_asset('scripts/ui-taburl.js') }}"></script>
<script src="{{ secure_asset('scripts/app.js') }}"></script>
<script src="{{ secure_asset('scripts/ajax.js') }}"></script>
<!-- endbuild -->
<script>
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
</script>
<script type="text/javascript">
    var reformalOptions = {
        project_id: 976291,
        show_tab: false,
        project_host: "SocNotify.reformal.ru"
    };
    
    (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript'; script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
    })();
</script><noscript><a href="http://reformal.ru"><img src="http://media.reformal.ru/reformal.png" /></a><a href="http://SocNotify.reformal.ru">Предложения</a></noscript>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>

<!-- VK Widget -->
<div id="vk_community_messages"></div>
<script type="text/javascript">
VK.Widgets.CommunityMessages("vk_community_messages", 133937532, {expanded: "1",tooltipButtonText: "Есть вопрос?"});
</script>
</body>
</html>

@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8">
            @foreach($articles as $article)
                <div class="box">
                    <div class="box-header b-b">
                        <h3>{{ $article->title }}</h3>
                    </div>
                    <ul class="list">
                        <li class="list-item">
                            <div class="list-body">
                                <div class="text-ellipsis">{{ $article->body }}</div>
                                <small class="block text-muted"><i class="fa fa-fw fa-clock-o"></i>{{ $article->created_at }}</small>
                            </div>
                        </li>
                    </ul>
                </div>
            @endforeach
        </div>
        <div class="col-md-4">
        @if($user->status === 'registered')
        	<div class="box">
        <div class="box-header light">
          <h3>Подарок</h3>
          <small>Выберите свой подарок</small>
        </div>
        <div class="box-body">
          <p class="m-a-0">
          <center><img src="http://s1.iconbird.com/ico/0512/2experts30iconsset/w512h5121337868883Box.png" width="160" align="center"><br>
          Выберите один из двух <b>бесплатных</b> подарков от сервиса SocNotify.<br><hr>
          <button class="btn btn-outline rounded b-info text-info" onclick="Gift.SendGift('money');">150 рублей на баланс</button><br><hr>
          <button class="btn btn-outline rounded b-info text-info" onclick="Gift.SendGift('trial');">3 дня использования сервиса</button>
          </center>
          </p>
        </div>
      </div>
        @endif
        <iframe src="https://tgwidget.com/widget/?id=585c4c86007b25867b8b4567" frameborder="0" scrolling="no" horizontalscrolling="no" verticalscrolling="no" width="100%" height="400px" async></iframe></div>
    </div>

@endsection
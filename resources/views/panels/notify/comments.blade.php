@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header light lt">
            <h3>{{ $title }}</h3>
            <small>Комментарии из сообщества</small>
        </div>
    </div>

<div class="row">
    <div class="col-md-4">
        <div class="nav-active-border b-info left box">
            <ul class="nav nav-sm">
                <li class="nav-item">
                    <a href="{{ secure_url('/home/groups/notify/') }}/{{ $type }}/{{ $id }}/new" class="nav-link">
                        Новые комментарии
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ secure_url('/home/groups/notify/') }}/{{ $type }}/{{ $id }}/old" class="nav-link" href="#">
                        Прочитанные
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ secure_url('/home/groups/notify/') }}/{{ $type }}/{{ $id }}/favorite" class="nav-link" href="#">
                        Важные
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-8" style="overflow-x:hidden;">
        @foreach($callbacks as $callback)
            <div id="{{ $callback->id }}" style="width: 320px; float: left; margin-left: 40px;">
                <div class="box text-left">
                    <div class="p-a-md text-center">
                        <p><img src="{{ $callback->author_avatar }}" alt="." class="img-circle w-56"></p>
                        <a href="#" class="text-md block">{{ $callback->author_name }}</a>
                        <p>{{ $callback->text }}</p>
                        <a href="{{ $callback->link }}" target="_blank" class="btn btn-outline rounded b-info text-info">Ответить</a><br>
                        <span class="label rounded">{{ $callback->created_at }}</span>
                    </div>
                    <div class="row no-gutter b-t">
                        <div class="col-xs-4 b-r">
                            <a class="p-a block text-center @if($callback->status === 'old') active @endif" @if($callback->status != 'old')onclick="CallBack.worker('{{ $callback->id }}', 'do_archive');"@endif>
                                <i class="material-icons md-24 text-muted m-v-sm inline">spellcheck_outline</i>
                                <i class="material-icons md-24 text-danger m-v-sm none">spellcheck</i>
                                <span class="block">Прочитанно</span>
                            </a>
                        </div>
                        <div class="col-xs-4 b-r">
                            <a class="p-a block text-center @if($callback->favorite == '0') active @endif" @if($callback->favorite == '0')onclick="CallBack.worker('{{ $callback->id }}', 'do_favorite_true');" @else onclick="CallBack.worker('{{ $callback->id }}', 'do_favorite_false');" @endif data-ui-toggle-class="">
                                <i class="material-icons md-24 text-muted m-v-sm none">star_border</i>
                                <i class="material-icons md-24 text-danger m-v-sm inline">star</i>
                                <span class="block">Избранное</span>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <a class="p-a block text-center" data-ui-toggle-class="" onclick="CallBack.worker('{{ $callback->id }}', 'do_delete');">
                                <i class="material-icons md-24 text-muted m-v-sm inline">delete_outline</i>
                                <i class="material-icons md-24 text-danger m-v-sm none">delete</i>
                                <span class="block">Удалить</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
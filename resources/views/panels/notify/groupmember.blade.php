@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header light lt">
            <h3>{{ $StaticData['title'] }}</h3>
            <small>Информация о участниках сообщества</small>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 b-r light lt">
            <div class="p-a-md">
                <span class="pull-right text-success">{{ $StaticData['join_count'] }}</span>
                <small>Вступившие</small>
                <div class="progress progress-xs m-t-sm white bg">
                    <div class="progress-bar success" data-toggle="tooltip" data-original-title="{{ $StaticData['join_percent'] }}%" style="width: {{ $StaticData['join_percent'] }}%"></div>
                </div>
                <span class="pull-right text-danger">{{$StaticData['unjoin_count']}}</span>
                <small>Вышедшие</small>
                <div class="progress progress-xs m-t-sm white bg">
                    <div class="progress-bar danger" data-toggle="tooltip" data-original-title="{{ $StaticData['unjoin_percent'] }}%" style="width: {{ $StaticData['unjoin_percent'] }}%"></div>
                </div>
                <p class="text-muted m-t-md text-sm">Статистика по участникам сообщества</p>
            </div>
        </div>
        <div class="col-md-8">
            @foreach($callbacks as $callback)
                <div style="width: 320px; float: left; margin-left: 40px;">
                    <div class="box text-left">
                        <div class="p-a-md text-center">
                            <p><img src="{{ $callback->author_avatar }}" alt="." class="img-circle w-56"></p>
                            <a href="#" class="text-md block">{{ $callback->author_name }}</a>
                            <p>@if($callback->sub_action == 'group_leave')<span class="label rounded danger">Вышел</span> @else <span class="label rounded success">Вступил</span> @endif</p>
                            <p>{{ $callback->created_at }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
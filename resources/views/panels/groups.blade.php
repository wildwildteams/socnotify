@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h2>Ваши сообщества</h2>
            <small>
                Настройка ваших сообществ
            </small>
        </div>
        <table class="table table-striped b-t">
            <thead>
            <tr>
                <th>Фото</th>
                <th>Название</th>
                <th>Данные</th>
                <th>Ссылка</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($groups as $group)
                <tr id="{{ $group->id }}">
                    <td>
                        <span class="w-40 avatar circle pink">
                            <img src="{{ $group->avatar }}" />
                        </span>
                    </td>
                    <td style="max-width: 200px;text-decoration: underline;"><a href="{{ secure_url('/home/groups/show/stats') }}/{{ $group->id }}">{{ $group->title }}</a></td>
                    <td>
                        @widget('GroupButtonsNotify', ['group_id' => $group->id])
                    </td>
                    <td><input type="text" class="form-control" value="{{ secure_url('api/callback/') }}/{{ $group->callback_link }}"></td>
                    <td><div class="dropdown inline">
                            <button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Выбор</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{secure_url('/home/groups/settings/')}}/{{ $group->id }}">Настройки</a>
                                <div class="dropdown-divider"></div>
                                <a onclick="groups.DelGroupById('{{$group->id}}')" class="dropdown-item">Удалить</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <center>
        <button class="btn btn-outline b-info text-info" data-toggle="modal" data-target="#m-sm">Добавить сообщество</button>
    </center>

    <!-- small modal -->
    <div id="m-sm" class="modal" data-backdrop="true">
        <div class="row-col h-v">
            <div class="row-cell v-m">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Добавить группу</h5>
                        </div>
                        <div class="modal-body text-left p-lg">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Идентификатор группы (group_id) или короткое имя</label>
                                <input type="text" class="form-control" id="indetif" placeholder="Индитификатор">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Строка, которую должен вернуть сервер:</label>
                                <input type="text" class="form-control" id="vkreposne" placeholder="Строка">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Отменить</button>
                            <button type="button" class="btn danger p-x-md" data-dismiss="modal" onclick="groups.add()">Добавить</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
            </div>
        </div>
    </div>
    <!-- / .modal -->
@endsection
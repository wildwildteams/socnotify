@extends('layouts.app')

@section('content')
    <div class="box p-a">
        <div class="pull-left m-r">
            <span class="avatar w-40 text-center rounded primary">
              <span class="fa fa-dollar"></span>
            </span>
        </div>
        <div class="clear">
            <h4 class="m-a-0 text-md">Ваш баланс: {{ $user->balance }}р</h4>
            <small class="text-muted">Ваш тариф <span class="label primary">@if($user->tariff == 1)1 Сообщество @elseif($user->tariff == 2)3 Сообщества @elseif($user->tariff == 3) Безлимит @endif</span> активен до: <b><span class="label primary">{{ $user->tariff_finish }}</span></b></small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5"><iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/quickpay/shop-widget?account=410013339771683&quickpay=shop&payment-type-choice=on&mobile-payment-type-choice=on&label={{ $user->id }}&writer=seller&targets=SocNotify+-+%D0%BF%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5+%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81%D0%B0&targets-hint=&default-sum=49&button-text=01&successURL=https%3A%2F%2Fsocnotify.online%2Fhome%2Ftariff" width="450" height="198"></iframe></div>
        <div class="col-md-4"><div class="box">
                <div class="box-header light lt">
                    <h3>Купить тариф</h3>
                    <small>Выберите тариф, и оплатите со своего баланса</small>
                </div>
                <div class="box-body">
                    <p class="m-a-0"><select id="tarif" class="form-control c-select">
                            <option value="1">1 Сообщество - 249р</option>
                            <option value="2">3 Сообщества - 549р</option>
                            <option value="3">Безлимит - 849р</option>
                        </select>
                    <center>
                    <br>
                    <p>Стоимость указанна за месяц использования</p>
                    <button onclick="Payment.BuyTariff();" class="btn btn-fw info">Оплатить</button>
                    </p>
                    </center>
                </div>
            </div></div>
        <div class="col-md-3"><div class="box">
                <div class="box-header light lt">
                    <h3>Активируйте купон</h3>
                    <small>Получите деньги на баланс</small>
                </div>
                <div class="box-body">
                    <p class="m-a-0">
                    <input type="text" class="form-control"><br>
                    <button class="btn btn-fw info">Активировать</button>
                    </p>
                </div>
            </div></div>
    </div>

    <div class="box">
        <div class="box-header">
            <h2>Статистика</h2>
            <small>
                Статистика активности вашего баланса
            </small>
        </div>
        <table class="table table-striped b-t">
            <thead>
            <tr>
                <th>#</th>
                <th>Баланс</th>
                <th>Действие</th>
                <th>Описание</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td><span class="label rounded success">+200</span></td>
                <td>Пополнение</td>
                <td>Пополнение баланса с yandex.money</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
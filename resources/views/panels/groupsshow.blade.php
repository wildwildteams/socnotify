@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header light lt">
            <h3>{{ $group->title }}</h3>
            <small>Статистика сообщества</small>
        </div>
        <div class="box-body">
            <p class="m-a-0">
                Статистика
            </p>
        </div>
    </div>
@endsection
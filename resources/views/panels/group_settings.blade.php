@extends('layouts.app')

@section('content')
    <input id="group_id" type="text" value="{{ $group->id }}" style="visibility: hidden">
    <div class="box">
        <div class="box-header light lt">
            <h3>{{ $group->title }}</h3>
            <small>Настройки сообщества</small>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            @if($GroupSettings->telegram_id == '')
                <div class="box blue">
                    <div class="box-header">
                        <h3>Привязать Telegram для уведомлений</h3>
                        <small>Следуйте инструкции. </small>
                    </div>
                    <div class="box-body b-t">
                        <p class="m-a-0">Добавьте бота в друзья <a href="#"><b>@SocNotifyBot</b></a><br>
                            Отправьте ему сообщение
                            <input type="text" class="form-control" value="/connect [{{$GroupSettings->telegram_code}}]">
                        </p>
                    </div>
                </div>
            @endif
            <div class="box">
                <div class="box-header light lt">
                    <h3>BlackList</h3>
                    <small>Список ID ВКонтакте, через запятую, уведомления от которых система будет игнорировать</small>
                </div>
                <div class="box-body">
                    <p class="m-a-0"><textarea id="black_list" class="form-control" rows="2">{{ $GroupSettings->black_list }}</textarea></p>
                </div>
            </div>
            <div class="box">
                <div class="box-header light lt">
                    <h3>Время работы</h3>
                    <small>Укажите время в котороё вы хотите получать уведомления (важно ознакомится с FAQ)</small>
                </div>
                <div class="box-body">
                    <p class="m-a-0">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" id="sleep_from" class="form-control" placeholder="От (например 07:00)" value="{{ $GroupSettings->sleep_from }}">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="sleep_to" class="form-control" placeholder="До (например 23:00)" value="{{ $GroupSettings->sleep_to }}">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            @if($GroupSettings->push_api_data == '')
                <div class="box">
                    <div class="box-header light lt">
                        <h3>Привязка Push уведомлений</h3>
                        <small>Подпишитесь на рассылку уведомлений</small>
                    </div>
                    <div class="box-body">
                        <iframe frameborder="0" src="https://pushall.ru/widget.php?subid=2882" width="320" height="120" scrolling="no" style="overflow: hidden;">
                        </iframe>
                    </div>
                </div>
            @endif
            <div class="box">
                <div class="box-header light lt">
                    <h3>Уведомления</h3>
                    <small>Активация уведомлений</small>
                </div>
                <div class="box-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td width="180">
                                <select id="send_sms" class="form-control c-select">
                                    @if($GroupSettings->send_sms != 1)
                                        <option value="0">Отключенны</option>
                                        <option value="1">Включенны</option>
                                    @else
                                        <option value="1">Включенны</option>
                                        <option value="0">Отключенны</option>
                                    @endif
                                </select>
                            </td>
                            <td>SMS</td>
                        </tr>
                        <tr>
                            <td><select id="send_email" class="form-control c-select">
                                    @if($GroupSettings->send_email != 1)
                                        <option value="0">Отключенны</option>
                                        <option value="1">Включенны</option>
                                    @else
                                        <option value="1">Включенны</option>
                                        <option value="0">Отключенны</option>
                                    @endif
                            </select>
                            </td>
                            <td>Email</td>
                        </tr>
                        <tr>
                            <td>
                                <select id="send_telegram" class="form-control c-select">
                                    @if($GroupSettings->send_telegram != 1)
                                        <option value="0">Отключенны</option>
                                        <option value="1">Включенны</option>
                                    @else
                                        <option value="1">Включенны</option>
                                        <option value="0">Отключенны</option>
                                    @endif
                                </select>
                            </td>
                            <td>Telegram</td>
                        </tr>
                        <tr>
                            <td>
                                <select id="send_push" class="form-control c-select">
                                    @if($GroupSettings->send_push != 1)
                                        <option value="0">Отключенны</option>
                                        <option value="1">Включенны</option>
                                    @else
                                        <option value="1">Включенны</option>
                                        <option value="0">Отключенны</option>
                                    @endif
                                </select>
                            </td>
                            <td>Push уведомления</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header light lt">
                    <h3>Уведомления</h3>
                    <small>Настройка уведомлений</small>
                </div>
                <div class="box-body">
                    <input type="text" id="sms_num" class="form-control" placeholder="Номер для уведомлений" value="{{ $GroupSettings->sms_num }}">
                    <hr>
                    <input type="text" id="email" class="form-control" placeholder="Email для уведомлений" value="{{ $GroupSettings->email }}">
                    <hr>
                    <input class="form-control" type="text" placeholder="Telegram логин" readonly="" value="{{ $GroupSettings->telegram_login }}">
                    <hr>
                    <input class="form-control" type="text" placeholder="Push логин" readonly="" value="{{ $GroupSettings->push_api_data }}">
                    <hr>
                </div>
            </div></div>
    </div>
    <button class="btn btn-fw info" onclick="Settings.save();">Сохранить</button>
@endsection
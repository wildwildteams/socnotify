@extends('layouts.app')

@section('content')

 <div class="row no-gutter">
			<div class="col-xs-6 col-sm-3 b-r b-b">
				<div class="padding">
					<div>
						<span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
					</div>
					<div class="text-center">
						<h2 class="text-center _600">{{ $GroupCount }}</h2>
						<p class="text-muted m-b-md">Всего сообществ</p>
					</div>
				</div>
	        </div>
	        <div class="col-xs-6 col-sm-3 b-r b-b">
				<div class="padding">
					<div>
					<span class="text-muted l-h-1x"><i class="ion-document text-muted"></i></span>
					</div>
					<div class="text-center">
						<h2 class="text-center _600">{{ $CallBacksCount }}</h2>
						<p class="text-muted m-b-md">Всего уведомлений</p>
					</div>
				</div>
	        </div>
	        <div class="col-xs-6 col-sm-3 b-r b-b">
				<div class="padding">
					<div>
						<span class="text-muted l-h-1x"><i class="ion-pie-graph text-muted"></i></span>
					</div>
					<div class="text-center">
						<h2 class="text-center _600">{{ $CallBacksCountNew }}</h2>
						<p class="text-muted m-b-md">Всего новых уведомлений</p>
					</div>
				</div>
	        </div>
	        <div class="col-xs-6 col-sm-3 b-b">
				<div class="padding">
					<div>
						<span class="text-muted l-h-1x"><i class="ion-paper-airplane text-muted"></i></span>
					</div>
					<div class="text-center">
						<h2 class="text-center _600">{{ $CallBacksCountFavorite }}</h2>
						<p class="text-muted m-b-md">Всего избранных уведомлений</p>
					</div>
				</div>
	        </div>
        </div>
        <center><p style="color: gray; padding-top: 25px;">Данные для более подробной статистики еще не собранны</p></center>
@endsection
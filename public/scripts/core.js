var host = 'https://socnotify.online/';

var UrlGo = {
    go: function (url) {
        window.location.replace(host+url);
    }
}

var groups = {
    add: function () {
        var result = null,
            indetif = $('#indetif').val(),
            vkresponse = $('#vkreposne').val();
        if (indetif.length > 4 && vkresponse.length > 4) {
            $.ajax({
                url: '../../ajax/AddGroup',
                type: 'POST',
                data: {
                    indetif: indetif,
                    vkresponse: vkresponse,
                }
            })
                .done(function(jqXHR) {
                    if (jqXHR.status != 'ok') {
                        toastr.error('Ошибка: '+jqXHR.error_text);
                    } else {
                        UrlGo.go('home/groups');
                    }
                })
                .fail(function(jqXHR) {
                    if (jqXHR.status == 422) {
                        toastr.error('Сервер вернул ошибку при добавлении группы');
                    }
                });
        } else {
            toastr.error('Данные для добавления группы указанны на верно')
        }
    },
    DelGroupById: function (id) {
        $.ajax({
            url: '../../ajax/DeleteGroupById',
            type: 'POST',
            data: {
                id: id,
            }
        })
            .done(function(jqXHR) {
                if (jqXHR.status != 'ok') {
                    toastr.error('Ошибка: '+jqXHR.error_text);
                } else {
                    toastr.success('Успешно удалили сообщество');
                    $("#"+id).remove();
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 422) {
                    toastr.error('Сервер вернул ошибку :422');
                }
            });
    }
}

var CallBack = {
    worker:function (id, method) {
        $.ajax({
            url: '../../../../../../ajax/WorkWithCallback',
            type: 'POST',
            data: {
                id: id,
                method: method,
            }
        })
            .done(function(jqXHR) {
                if (jqXHR.status != 'ok') {
                    toastr.error('Ошибка: '+jqXHR.error_text);
                } else {
                    switch (method) {
                        case 'do_archive':
                            $('#'+id).remove();
                        break;
                        case 'do_delete':
                            $('#'+id).remove();
                        break;
                    }
                    toastr.success('Успешно выполнили действие');
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 422) {
                    toastr.error('Сервер вернул ошибку');
                }
            });
    }
}

var Settings = {
    save: function () {
        var black_list = $('#black_list').val(),
            send_sms = $('#send_sms').val(),
            send_email = $('#send_email').val(),
            send_telegram = $('#send_telegram').val(),
            send_push = $('#send_push').val(),
            sms_num = $('#sms_num').val(),
            email = $('#email').val(),
            sleep_from = $('#sleep_from').val(),
            sleep_to = $('#sleep_to').val(),
            group_id = $('#group_id').val();
        $.ajax({
            url: '../../../../ajax/UpdateGroup',
            type: 'POST',
            data: {
                black_list: black_list,
                send_sms: send_sms,
                send_email: send_email,
                send_telegram: send_telegram,
                send_push: send_push,
                sms_num: sms_num,
                email:  email,
                sleep_from: sleep_from,
                sleep_to: sleep_to,
                group_id: group_id,
            }
        })
            .done(function(jqXHR) {
                if (jqXHR.status != 'ok') {
                    toastr.error('Ошибка при сохранении данных');
                } else {
                    toastr.success('Успешно сохранили данные');
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 422) {
                    toastr.error('Сервер вернул ошибку');
                }
            });
    }
}

var Payment = {
    BuyTariff: function () {
        $.ajax({
            url: '../../ajax/PayForTariff',
            type: 'POST',
            data: {
                id: $('#tarif').val(),
            }
        })
            .done(function(jqXHR) {
                if (jqXHR.status != 'ok') {
                    toastr.error('Ошибка: '+jqXHR.error_text);
                } else {
                    toastr.success('Успешно оплатили тариф');
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 422) {
                    toastr.error('Сервер вернул ошибку :422');
                }
            });
    }
}

var Gift = {
    SendGift: function(GiftType) {
        $.ajax({
            url: '../ajax/GetGift',
            type: 'POST',
            data: {
                type: GiftType,
            }
        })
            .done(function(jqXHR) {
                if (jqXHR.status != 'ok') {
                    toastr.error('Ошибка: '+jqXHR.error_text);
                } else {
                    toastr.success('Успешно получили подарок');
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 422) {
                    toastr.error('Сервер вернул ошибку :422');
                }
            });
    }
}
<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Article;
use App\Group;
use App\CallBack;
use App\Setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        $user = User::find(\Auth::id());
        return view('panels/main', ['articles' => $articles, 'user' => $user]);
    }

    public function ShowGroupSettings($id) {
        $group = Group::find($id);

        if ((!$group) or $group->owner_id != \Auth::id()) {
            return \Redirect::secure('home/groups');
        }

        $GroupSettings = Setting::where('community_id', '=', $id)->first();

        return view('panels/group_settings', ['group' => $group, 'GroupSettings' => $GroupSettings]);
    }

    public function ShowGroupsPage() {
        $groups = Group::where('owner_id', '=', \Auth::id())->get();
        return view('panels/groups', ['groups' => $groups]);
    }

    public function ShowStatsPage() {
        $groups = Group::where('owner_id', '=', \Auth::id())->get();
        $GroupCount = $groups->count();
        $CallBacksCount = 0;
        $CallBacksCountNew = 0;
        $CallBacksCountFavorite = 0;
        for ($i = 0; $i < $GroupCount; $i++) {
            $data = CallBack::where('community_id', '=', $groups[$i]->id)->get();
            $CallBacksCount = $CallBacksCount + $data->count();
        }
        for ($i = 0; $i < $GroupCount; $i++) {
            $data = CallBack::where('community_id', '=', $groups[$i]->id)->where('status', '=', 'new')->get();
            $CallBacksCountNew = $CallBacksCountNew + $data->count();
        }
        for ($i = 0; $i < $GroupCount; $i++) {
            $data = CallBack::where('community_id', '=', $groups[$i]->id)->where('favorite', '=', '1')->get();
            $CallBacksCountFavorite = $CallBacksCountFavorite + $data->count();
        }
        return view('panels/stats', ['groups' => $groups, 'GroupCount' => $GroupCount, 'CallBacksCount' => $CallBacksCount, 'CallBacksCountNew' => $CallBacksCountNew,'CallBacksCountFavorite' => $CallBacksCountFavorite]);
    }

    public function ShowNotifyPage() {
        return view('panels/notify');
    }

    public function ShowTariffPage() {
        $user = User::find(\Auth::id());
        return view('panels/tariff', ['user' => $user]);
    }

    public function ShowGroupById($id) {
        $group = Group::where('id', '=', $id)->first();
        return view('panels.groupsshow', ['group' => $group]);
    }
    
    public function ShowNotification($type, $id, $status) {
        // Получаем информацию о группе
        $group = Group::find($id);

        // Проверяем наличие группы, и принадлежит ли она авторизированному пользователю
        if ((!$group) or $group->owner_id != \Auth::id()) {
            return \Redirect::secure('home/groups');
        }

        // Генерируем запрос
        switch ($status) {
            case 'new':
                $callbacks = CallBack::where('community_id', '=', $id)
                                        ->where('action', '=', $type)
                                        ->where('status', '=', 'new')
                                        ->orderBy('unix_data','desc')->get();
            break;
            case 'old':
                $callbacks = CallBack::where('community_id', '=', $id)
                    ->where('action', '=', $type)
                    ->where('status', '=', 'old')
                    ->orderBy('unix_data','desc')->get();
            break;
            case 'favorite':
                $callbacks = CallBack::where('community_id', '=', $id)
                    ->where('action', '=', $type)
                    ->where('favorite', '=', true)
                    ->orderBy('unix_data','desc')->get();
            break;
        }

        // Генерируем вьюшку
        switch ($type) {
            case 'comment':
                return view('panels.notify.comments', ['callbacks' => $callbacks, 'id' => $id, 'type' => $type, 'status' => $status, 'title' => $group->title]);
            break;
            case 'message':
                return view('panels.notify.message', ['callbacks' => $callbacks, 'id' => $id, 'type' => $type, 'status' => $status, 'title' => $group->title]);
            break;
            case 'wall':
                return view('panels.notify.wall', ['callbacks' => $callbacks, 'id' => $id, 'type' => $type, 'status' => $status, 'title' => $group->title]);
            break;
            case 'board':
                return view('panels.notify.board', ['callbacks' => $callbacks, 'id' => $id, 'type' => $type, 'status' => $status, 'title' => $group->title]);
            break;
            case 'group':
                $join_count = 0;
                $unjoin_count = 0;
                if ($callbacks->count() > 0) {
                    for ($i = 0; $i < $callbacks->count(); $i++) {
                        if ($callbacks[$i]->sub_action == 'group_leave') {
                            $unjoin_count++;
                        } else {
                            $join_count++;
                        }
                    }
                    $join_percent = ($join_count/($join_count+$unjoin_count))*100;
                    $unjoin_percent = ($unjoin_count/($join_count+$unjoin_count))*100;

                    $StaticData = array('join_count' => $join_count,
                        'unjoin_count' => $unjoin_count,
                        'join_percent' => $join_percent,
                        'unjoin_percent' => $unjoin_percent,
                        'title' => $group->title
                    );
                    return view('panels.notify.groupmember', ['StaticData' => $StaticData, 'callbacks' => $callbacks]);
                } else {
                    return \Redirect::secure('home/groups');
                }
            break;
        }

    }
}

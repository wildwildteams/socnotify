<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PayForTariffRequest;
use App\User;
use Carbon\Carbon;

class TariffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function PayForTariff(PayForTariffRequest $request) {
        switch ($request->input('id')) {
            case '1': $price = 249; break;
            case '2': $price = 549; break;
            case '3': $price = 849; break;
        }
        $user = User::find(\Auth::id());
        if ($user->balance >= $price) {
            $user->balance = $user->balance - $price;
            if ($user->tariff_finish > Carbon::now()) {
                // Если уже есть активированный тариф
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $user->tariff_finish);
                $user->tariff_finish = $date->addDay(31);
            } else {
                // Если нету активированного тарифа
                $user->tariff_finish = Carbon::now()->addDay(31);
            }
            $user->tariff = $request->input('id');
            if($user->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
            }
        } else {
            return response()->json(['status' => 'error', 'error_text' => 'Недостаточно средств']);
        }
    }
}

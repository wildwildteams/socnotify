<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddGroupRequest;
use ATehnix\VkClient\Client;
use App\Group;
use App\User;
use App\Setting;
use App\Http\Requests\DeleteGroupByIdRequest;
use Carbon\Carbon;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function GroupAdd(AddGroupRequest $request) {
        // Проверка на тариф
        $user = User::find(\Auth::id());
        if ($user->tariff_finish <= Carbon::now()) {
            return response()->json(['status' => 'error', 'error_text' => 'Срок действия вашего тарифа истек']);
        }

        // Проверка на количество групп на тариф
        $GroupCount = Group::where('owner_id', '=', \Auth::id())->get();
        $GroupCount = $GroupCount->count();
        switch ($user->tariff) {
            case 1: $MaxGroup = 1; break;
            case 2: $MaxGroup = 3; break;
            case 3: $MaxGroup = 100; break;
        }
        if ($GroupCount >= $MaxGroup) {
            return response()->json(['status' => 'error', 'error_text' => 'Ваш тариф не позволяет добавить еще одно сообщество']);
        }

        // Получаем id, title, avatar
        $api = new Client();
        try {
            $response = $api->request('groups.getById', ['group_id' => $request->input('indetif')]);
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'group_ids is undefined')) {
                return 'Такой группы не существует';
            }
        }

        // Получаем обьект пользователя
        $user = User::find(\Auth::id());

        $group = new Group();
        $group->title = $response['response'][0]['name'];
        $group->avatar = $response['response'][0]['photo_200'];
        $group->group_id = $response['response'][0]['id'];
        $group->owner_id = \Auth::id();
        $group->response_code = $request->input('vkresponse');
        $group->black_list = $user->vk_id;
        $group->callback_link = $response['response'][0]['id'].'_'.\Auth::id();
        if ($group->save()) {
            $Setting = new Setting();
            $Setting->community_id = $group->id;
            $Setting->owner_id = \Auth::id();
            $Setting->telegram_code = md5($group->id+\Auth::id());
            if ($Setting->save()) {
                return response()->json(['status' => 'ok', 'id' => $group->id, 'title' => $group->title, 'avatar' => $group->avatar]);
            } else {
                return response()->json(['status' => 'error']);
            }
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function DeleteGroupById(DeleteGroupByIdRequest $request) {
        $group = Group::find($request->input('id'));
        if ($group) {
            if ($group->owner_id == \Auth::id()) {
                if($group->delete()) {
                    return response()->json(['status' => 'ok']);
                } else {
                    return response()->json(['status' => 'error', 'error_text' => 'Ошибка при удалении сообщества']);
                }
            } else {
                return response()->json(['status' => 'error', 'error_text' => 'Сообщество не принадлежит вам']);
            }
        } else {
            return response()->json(['status' => 'error', 'error_text' => 'Не смогли получить данные о сообществе']);
        }
    }

    public function UpdateGroupSettings(request $request) {
        $group = Group::find($request->input('group_id'));

        // Проверяем наличие группы, и принадлежит ли она авторизированному пользователю
        if ((!$group) or $group->owner_id != \Auth::id()) {
            return 'error';
        } else {
            $Setting = Setting::where('community_id', '=', $request->input('group_id'))->first();
            $Setting->black_list = $request->input('black_list');
            $Setting->send_sms = $request->input('send_sms');
            $Setting->send_email = $request->input('send_email');
            $Setting->send_telegram = $request->input('send_telegram');
            $Setting->send_push = $request->input('send_push');
            $Setting->sms_num = $request->input('sms_num');
            $Setting->email = $request->input('email');
            if ($request->input('sleep_to')) {$Setting->sleep_to = $request->input('sleep_to');}
            if ($request->input('sleep_from')) {$Setting->sleep_from = $request->input('sleep_from');}
            if ($Setting->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error']);
            }
        }
    }
}

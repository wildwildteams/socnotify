<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Request;
use Socialite;
use Carbon\Carbon;
use Auth;
use Redirect;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Redirect the user to the Vkontakte authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('vkontakte')->redirect();
    }

    /**
     * Obtain the user information from Vkontakte.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('vkontakte')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/vkontakte');
        }

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return Redirect::to('home');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $vkontakeUser
     * @return User
     */
    private function findOrCreateUser($vkontakteUser)
    {
        if ($authUser = User::where('vk_id', $vkontakteUser->id)->first()) {
            return $authUser;
        }

        //exit($tariffdate);
        $RegUser =  User::create([
            'name' => $vkontakteUser->name,
            'vk_id' => $vkontakteUser->id,
            'avatar' => $vkontakteUser->avatar,
            'referer' => '',
            'ip' => Request::ip(),
            'balance' => 0,
            'tariff' => 1,
            'tariff_finish' => Carbon::now(),
            'status' => 'registered'
        ]); 

        return $RegUser;
    }
}

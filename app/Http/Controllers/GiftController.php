<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GiftRequest;
use App\User;
use Carbon\Carbon;

class GiftController extends Controller
{
    public function GetGift(GiftRequest $request) {
    	$user =  User::find(\Auth::id());
        if ($user->status === 'ok') return response()->json(['status' => 'error', 'error_text' => 'Вы уже получали свой подарок']); 
    	if ($request->input('type') === 'trial') {
    		// Если выбранно триальное время
    		$user->tariff_finish = Carbon::now()->addDay(3);
    	} elseif ($request->input('type') === 'money') {
    		// Если выбранна капуста
    		$user->balance = $user->balance + 150;
    	}
        $user->status = 'ok';
    	if($user->save()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
            }
    }
}

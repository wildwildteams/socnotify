<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\CallBack;
use App\Setting;
use ATehnix\VkClient\Client;
use App\Http\Requests\WorkerRequest;
use Carbon\Carbon;
use Telegram\Bot\Api;
use Storage;
use App\Jobs\SendTelegramNotify;
use App\Jobs\SendSmsCNotify;
use App\Jobs\SendEmailNotify;
use App\Jobs\SendPushNotify;

class CallBackController extends Controller
{
    public function Worker(WorkerRequest $request) {
        $notify = CallBack::find($request->input('id'));
        $group = Group::find($notify->community_id);
        if ($group->owner_id == \Auth::id()) {
            switch ($request->input('method')) {
                case 'do_delete':
                    if($notify->delete()) {
                        // Если успешно удалили
                        return response()->json(['status' => 'ok']);
                    } else {
                        // Если не удалили
                        return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
                    }
                break;
                case 'do_archive':
                    $notify->status = 'old';
                    if ($notify->save()) {
                        // Если заархивировали
                        return response()->json(['status' => 'ok']);
                    } else {
                        // Если произошла ошибка
                        return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
                    }
                break;
                case 'do_favorite_true':
                    $notify->favorite = true;
                    if ($notify->save()) {
                        // Если добавили в фавориты
                        return response()->json(['status' => 'ok']);
                    } else {
                        // Если произошла ошибка
                        return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
                    }
                break;
                case 'do_favorite_false':
                    $notify->favorite = false;
                    if ($notify->save()) {
                        // Если убрали из фаворитов
                        return response()->json(['status' => 'ok']);
                    } else {
                        // Если произошла ошибка
                        return response()->json(['status' => 'error', 'error_text' => 'Ошибка базы данных']);
                    }
                break;
            }
        } else {
            return response()->json(['status' => 'error', 'text' => 'Данный еллемент не принадлежит вам']);
        }
    }

    public function GetCallBack($link, Request $request) {
        $group = Group::where('callback_link', '=', $link)->first();
        if ($group) {

            // Получаем BlackList
            $setting = Setting::where('community_id', '=', $group->id)->first();
            $BlackList = $setting->black_list;
            // Получили BLackList

            $user = User::find($group->owner_id);
            if ($user->tariff_finish < Carbon::now()) {
                return 'ok';
                // И тут еще уведомляем пользователя о том что тарифчик то законился
            }

            if ($request->type == 'confirmation') {
                return $group->response_code;
            } else {
                switch ($request->type){
                    case 'wall_reply_new':
                        // Новый комментарий
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/wall-'.$request->group_id.'_'.$object['post_id'].'?reply='.$object['id'].'';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                                'link' => $link,
                                                'text' => $object['text'],
                                                'author_avatar' => $response['response'][0]['photo_50'],
                                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                                'action' => 'comment',
                                                'sub_action' => $request->type,
                                                'unix_data' => $object['date'],
                                                'status' => 'new',
                                                'favorite' => false);
                        }
                    break;
                    case 'wall_post_new':
                        // Новый пост на стене
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/wall'.$object['owner_id'].'_'.$object['id'];
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['text'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'wall',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                    case 'photo_comment_new':
                        // Новый комментарий под фото
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/photo'.$object['photo_owner_id'].'_'.$object['photo_id'].'';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['text'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'comment',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                    case 'video_comment_new':
                        // Новый комментарий под видео
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/video'.$object['video_owner_id'].'_'.$object['video_id'].'';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['text'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'comment',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                    case 'message_new':
                        // Новое сообщение сообществу
                        // Не оттестированно, не проверенно
                        $object = $request->object;
                            $link = 'https://vk.com/gim'.$request->group_id.'?sel='.$object['user_id'].'';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['user_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['user_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['body'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'message',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                    break;
                    case 'group_leave':
                        // Выход из сообщества
                        $object = $request->object;
                        if ($object['self'] == 1) {
                            // Если пользователь вышел самостоятельно
                            $link = 'https://vk.com/id'.$object['user_id'];
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['user_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['user_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => 'Пользователь покинул сообщество',
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'group',
                                'sub_action' => $request->type,
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                    case 'group_join':
                        // Вступление в группу
                        $object = $request->object;
                            // Если пользователь вышел самостоятельно
                            $link = 'https://vk.com/id'.$object['user_id'];
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['user_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['user_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => 'Пользователь вступил в сообщество',
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'].' '.$response['response'][0]['last_name'],
                                'action' => 'group',
                                'sub_action' => $request->type,
                                'status' => 'new',
                                'favorite' => false);
                    break;
                    case 'market_comment_new':
                        // Комментарий под товаром
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/market' . $object['market_owner_id'] . '?w=product' . $object['market_owner_id'] . '_' . $object['item_id'] . '';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['text'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'] . ' ' . $response['response'][0]['last_name'],
                                'action' => 'comment',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                    case 'board_post_new':
                        // создание нового комментария в обсуждении
                        $object = $request->object;
                        if ($object['from_id'] > 0) {
                            $link = 'https://vk.com/topic' . $object['topic_owner_id'] . '_' . $object['topic_id'] . '?post=' . $object['id'] . '';
                            $api = new Client();
                            try {
                                $response = $api->request('users.get', ['user_ids' => $object['from_id'], 'fields' => 'photo_50']);
                            } catch (\Exception $e) {
                                return 'Такого пользователя не существует';
                            }
                            $ActionUserId = $object['from_id'];
                            $PushParams = array('community_id' => $group->id,
                                'link' => $link,
                                'text' => $object['text'],
                                'author_avatar' => $response['response'][0]['photo_50'],
                                'author_name' => $response['response'][0]['first_name'] . ' ' . $response['response'][0]['last_name'],
                                'action' => 'board',
                                'sub_action' => $request->type,
                                'unix_data' => $object['date'],
                                'status' => 'new',
                                'favorite' => false);
                        }
                    break;
                }

                // Проверяем пользователя на наличие в BlackList
                $result = strpos ($BlackList, strval($ActionUserId));
                if ($result === FALSE) {
                    // Пользователя нету в BlackList
                } else {
                    // Пользователь есть в BlackList
                    return 'ok';
                }

                // Проверяем наличие скобочек
                $result = strpos ($PushParams['text'], '[');
                if ($result === FALSE) {

                } else {
                    $ret = '';
                    if (preg_match('~\[(id|club|public)(\d+)\|([^\]]+)\](.+)~iu', $PushParams['text'], $matches)) {
                        $ret = htmlspecialchars_decode("$matches[3]$matches[4]");
                    }
                    $PushParams['text'] = $ret;
                }

                // Добавляем данные обработанные после пуша
                $CallBack = new CallBack();
                $CallBack->community_id = $PushParams['community_id'];
                $CallBack->link = $PushParams['link'];
                $CallBack->text = $PushParams['text'];
                $CallBack->author_avatar = $PushParams['author_avatar'];
                $CallBack->author_name = $PushParams['author_name'];
                $CallBack->action = $PushParams['action'];
                $CallBack->sub_action = $PushParams['sub_action'];
                if (isset($PushParams['unix_data'])) {
                    $CallBack->unix_data = $PushParams['unix_data'];
                }
                $CallBack->status = $PushParams['status'];
                $CallBack->favorite = $PushParams['favorite'];
                if ($CallBack->save()) {
                    if (($setting->sleep_from) and ($setting->sleep_to)) {
                        if (Carbon::createFromFormat('H:i:s', date('H:i:s'))->between(Carbon::createFromFormat('H:i:s',$setting->sleep_to), Carbon::createFromFormat('H:i:s',$setting->sleep_from))) {
                            // Если время подходящее то запускаем рассылку уведомлений
                            $PushParams = array_merge($PushParams, array('action_user_id' => $ActionUserId, 'owner_id' => $group->owner_id));
                            if (($setting->send_sms === 1) and ($setting->sms_num != '')) {
                                // То отправляем SMS
                                $job = (new SendSmsCNotify($PushParams))->onQueue('SendSMS');
                                dispatch($job);
                            }
                            if (($setting->send_email === 1) and ($setting->email != '')) {
                                // То отправляем Email
                                $job = (new SendEmailNotify($PushParams))->onQueue('SendEmail');
                                dispatch($job);
                            }
                            if (($setting->send_telegram === 1) and ($setting->telegram_id != '')) {
                                // То отправляем Telegram
                                $job = (new SendTelegramNotify($PushParams))->onQueue('SendTelegram');
                                dispatch($job);
                            }
                            if (($setting->send_push === 1) and ($setting->push_api_data != '')) {
                                // То отправляем Push уведомления
                                $job = (new SendPushNotify($PushParams))->onQueue('SendPush');
                                dispatch($job);
                            }
                        }
                    } else {
                        // То же самое, но если время не указанно
                        $PushParams = array_merge($PushParams, array('action_user_id' => $ActionUserId, 'owner_id' => $group->owner_id));
                        if (($setting->send_sms === 1) and ($setting->sms_num != '')) {
                            // То отправляем SMS
                            $job = (new SendSmsCNotify($PushParams))->onQueue('SendSMS');
                            dispatch($job);
                        }
                        if (($setting->send_email === 1) and ($setting->email != '')) {
                            // То отправляем Email
                            $job = (new SendEmailNotify($PushParams))->onQueue('SendEmail');
                            dispatch($job);
                        }
                        if (($setting->send_telegram === 1) and ($setting->telegram_id != '')) {
                            // То отправляем Telegram
                            $job = (new SendTelegramNotify($PushParams))->onQueue('SendTelegram');
                            dispatch($job);
                        }
                        if (($setting->send_push === 1) and ($setting->push_api_data != '')) {
                            // То отправляем Push уведомления
                            $job = (new SendPushNotify($PushParams))->onQueue('SendPush');
                            dispatch($job);
                        }
                    }
                    return 'ok';
                } else {
                    return response()->json(['status' => 'error']);
                }
            }
        } else {
            return 'Nothing found';
        }
    }

    public function TelegramTest() {
        $telegram = new Api('271443674:AAHE0Tmul7A8FZk8IaF6RYu871w-u60EeT4');
        $response = $telegram->setWebhook([
            'url' => 'https://socnotify.online/api/callback/telegram/271443674:AAHE0Tmul7A8FZk8IaF6RYu871w-u60EeT4/Webhook',
        ]);
    }

    public function SetPushAllData(Request $request) {
        $sign = md5('39c69920ff0eebe3d2e07ebb61e57c30'.$request->pushalluserid.$request->time.$_SERVER['REMOTE_ADDR']);
        if ($sign === $request->sign) {
            Setting::where('owner_id', \Auth::id())->update(['push_api_data' => $request->pushalluserid]);
            return \Redirect::secure('home/groups');
        } else {
            return \Redirect::secure('home/groups');
        }

    }

    public function YandexRequest(Request $request) {
        Storage::put('yandex.txt', $request);
        $secret_key = '+JhalwnOx0xb8tu14hfvAns6';
 
    $sha1 = sha1($request->input('notification_type') . '&'. $request->input('operation_id'). '&' . $request->input('amount') . '&643&' . $request->input('datetime') . '&'. $request->input('sender') . '&' . $request->input('codepro') . '&' . $secret_key. '&' . $request->input('label') );
 
    if (($sha1 != $request->input('sha1_hash')) or ($request->input('codepro'))) {
        exit();
    }
 
    // тут код на случай, если проверка прошла успешно
    //Storage::put('sucseess.txt', $request);
    $user = User::find($request->input('label'));
    $user->balance = $user->balance + $request->input('withdraw_amount');
    $user->save();
 
exit();
    }

    public function TelegramWebHook($token,Request $request) {
        $telegram = new Api('271443674:AAHE0Tmul7A8FZk8IaF6RYu871w-u60EeT4');
        $updates = $telegram->getWebhookUpdates();
        $message = $updates->getMessage()->getText();
        $start= strpos($message, "[")+1;
        $finish= strpos($message, "]");
        $length= $finish-$start;
        $code=Substr($message, $start, $length);
        if (strlen($code) > 16) {
            $setting = Setting::where('telegram_code','=', $code)->first();
            if ($setting) {
                $group = Group::find($setting->community_id);
                $setting->telegram_id = $updates->getMessage()->getFrom()->getId();
                $setting->telegram_login = '@'.$updates->getMessage()->getFrom()->getUsername();
                if ($setting->save()) {
                    $telegram->sendMessage([
                        'text' => 'Подключенны уведомления о сообществе: <b>'.$group->title.'</b>',
                        'parse_mode' => 'HTML',
                        'chat_id'=> $updates->getMessage()->getFrom()->getId()
                    ]);
                } else {
                    $telegram->sendMessage([
                        'text' => '<b>Ошибка:</b> Не смогли сохранить данные!',
                        'parse_mode' => 'HTML',
                        'chat_id'=> $updates->getMessage()->getFrom()->getId()
                    ]);
                }
            } else {
                $telegram->sendMessage([
                    'text' => '<b>Ошибка:</b> Группы с таким ключём не обнаруженно!',
                    'parse_mode' => 'HTML',
                    'chat_id'=>$updates->getMessage()->getFrom()->getId()
                ]);
            }
        } else {
            $telegram->sendMessage([
                'text' => '<b>Информация:</b> <a href="https://socnotify.oline">Подключение уведомлений от группы</a>',
                'parse_mode' => 'HTML',
                'chat_id'=>$updates->getMessage()->getFrom()->getId()
            ]);
        }
    }
}

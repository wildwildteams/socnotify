<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table = 'callbacks';
}

<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\CallBack;
use DB;

class GroupButtonsNotify extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'group_id' => 1,
        'count_board' => 0,
        'count_comments' => 0,
        'count_messages' => 0,
        'count_wall' => 0,
        'count_join' => 0
    ];
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $notify = CallBack::select(DB::raw('COUNT(*) as count, action'))->where('community_id',$this->config['group_id'])->where('status','new')->groupBy('action')->get();
        
        for ($i = 0; $i < count($notify); $i++) {
            switch ($notify[$i]['action']) {
                case 'comment':  $this->config['count_comments'] = $notify[$i]['count'];  break;
                case 'wall':  $this->config['count_wall'] = $notify[$i]['count'];  break;
                case 'message':  $this->config['count_messages'] = $notify[$i]['count'];  break;
                case 'group':  $this->config['count_join'] = $notify[$i]['count'];  break;
                case 'board':  $this->config['count_board'] = $notify[$i]['count'];  break;
            }
        }

        return view("widgets.group_buttons_notify", [
            'config' => $this->config,
        ]);
    }
}
<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Setting;

class SendEmailNotify implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $setting = Setting::where('community_id', '=', $this->data['community_id'])->first();
        
        Mail::send('emails.notify', array('text' => $this->data['text'], 'link' => $this->data['link'], 'user_name' => $this->data['author_name']), function($message) use ($setting)
        {
            $message->to($setting->email)->
                    from('reply@socnotify.online', 'SocNotify')->
                    subject('SocNotify - Уведомление в вашем сообществе');
        });
    }
}

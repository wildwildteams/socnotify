<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Group;
use App\Setting;

class SendPushNotify implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $setting = Setting::where('community_id', '=', $this->data['community_id'])->first();
        $group = Group::find($this->data['community_id']);

        curl_setopt_array($ch = curl_init(), array(
            CURLOPT_URL => "https://pushall.ru/api.php",
            CURLOPT_POSTFIELDS => array(
                "type" => "unicast",
                "id" => "2882",
                "key" => "39c69920ff0eebe3d2e07ebb61e57c30",
                "priority" => "1",
                "text" => "Новое действие в вашем сообществе",
                "title" => $group->title,
                "uid" => $setting->push_api_data,
            ),
            CURLOPT_RETURNTRANSFER => true
        ));
        $return=curl_exec($ch); //получить данные о рассылке
        curl_close($ch);
    }
}

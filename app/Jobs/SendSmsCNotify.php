<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use SMSCenter\SMSCenter;
use App\User;
use App\Setting;

class SendSmsCNotify implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->data['owner_id']);
        $setting = Setting::where('community_id', '=', $this->data['community_id'])->first();
        if ($user->balance >= 5) {
            $smsc = new \SMSCenter\SMSCenter('wildwildteams', md5('papalis1996'), false, [
                'charset' => SMSCenter::CHARSET_UTF8,
                'fmt' => SMSCenter::FMT_XML
            ]);
            // Отправка сообщения
            $result = $smsc->send($setting->sms_num, 'Новое уведомление в вашем сообществе', 'SocNotify');
            $user->balance = $user->balance-5;
            $user->save();
        } else {
            // Пишем Notify для пользователя
        }
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Telegram\Bot\Api;
use App\Group;
use App\CallBack;
use App\Setting;

class SendTelegramNotify implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $setting = Setting::where('community_id', '=', $this->data['community_id'])->first();
        $group = Group::find($this->data['community_id']);
        $TextAction = $this->GetActionType($this->data['sub_action']);
        $SendText = "<a href=\"https://vk.com/id".$this->data['action_user_id']."\">".$this->data['author_name']."</a> ".$TextAction." в <b>".$group->title. "</b>\n \n<i>".$this->data['text']."</i>\n \n<a href=\"".$this->data['link']."\">Ответить</a>";
        $telegram = new Api('271443674:AAHE0Tmul7A8FZk8IaF6RYu871w-u60EeT4');
        $telegram->sendMessage([
            'text' => $SendText,
            'parse_mode' => 'HTML',
            'chat_id'=> $setting->telegram_id,
            'disable_web_page_preview' => true
        ]);
    }

    private function GetActionType($sub_action) {
        switch ($sub_action){
            case 'wall_reply_new': $TextAction = 'оставил комментарий'; break;
            case 'wall_post_new': $TextAction = 'оставил новое сообщение на стене'; break;
            case 'photo_comment_new': $TextAction = 'оставил комментарий'; break;
            case 'video_comment_new': $TextAction = 'оставил комментарий'; break;
            case 'message_new': $TextAction = 'оставил сообщение'; break;
            case 'group_leave': $TextAction = 'покинул сообщество'; break;
            case 'group_join': $TextAction = 'вступил в сообщество'; break;
            case 'market_comment_new': $TextAction = 'оставил комментарий'; break;
            case 'board_post_new': $TextAction = 'оставил сообщение в обсуждении'; break;
        }
        return $TextAction;
    }
}
